package dialectquery

import (
	"bytes"
	"fmt"
	"text/template"
)

const (
	paramOnCluster    = "ON_CLUSTER"
	paramClusterMacro = "CLUSTER_MACRO"
	paramNoKeeperMap  = "NO_KEEPER_MAP"
)

type clusterParameters struct {
	OnCluster    bool
	NoKeeperMap  bool
	ClusterMacro string
}

type Clickhouse struct {
	Table          string
	CreateTableSQL string
	Params         clusterParameters
}

var _ Querier = (*Clickhouse)(nil)

var createTableTmpl = `
CREATE TABLE IF NOT EXISTS {{.TableName}}{{if .OnCluster}} ON CLUSTER '{{.ClusterMacro}}' {{end}}(
	version_id Int64,
	is_applied UInt8,
	date Date default now(),
	tstamp DateTime64(9, 'UTC') default now64(9, 'UTC')
)
ENGINE = {{if .NoKeeperMap}}MergeTree{{else}}{{if .OnCluster}}KeeperMap('/goose_version_repl'){{else}}KeeperMap('/goose_version'){{end}}{{end}}
PRIMARY KEY version_id
`

func (c *Clickhouse) CreateTable(tableName string) string {
	tmpl, err := template.New("create_table").Parse(createTableTmpl)
	if err != nil {
		// Note(Arun): It is not ideal but the function signature is part of an interface which will require rework if changed.
		return ""
	}
	buf := bytes.Buffer{}
	err = tmpl.Execute(&buf, struct {
		TableName    string
		ClusterMacro string
		OnCluster    bool
		NoKeeperMap  bool
	}{
		tableName,
		c.Params.ClusterMacro,
		c.Params.OnCluster,
		c.Params.NoKeeperMap,
	})
	if err != nil {
		// Note(Arun): It is not ideal but the function signature is part of an interface which will require rework if changed.
		return ""
	}
	return buf.String()
}

func (c *Clickhouse) InsertVersion(tableName string) string {
	q := `INSERT INTO %s (version_id, is_applied) VALUES ($1, $2)`
	return fmt.Sprintf(q, tableName)
}

func (c *Clickhouse) DeleteVersion(tableName string) string {
	q := `ALTER TABLE %s DELETE WHERE version_id = $1 SETTINGS mutations_sync = 2`
	return fmt.Sprintf(q, tableName)
}

func (c *Clickhouse) GetMigrationByVersion(tableName string) string {
	q := `SELECT tstamp, is_applied FROM %s WHERE version_id = $1 ORDER BY tstamp DESC LIMIT 1`
	return fmt.Sprintf(q, tableName)
}

func (c *Clickhouse) ListMigrations(tableName string) string {
	q := `SELECT version_id, is_applied FROM %s ORDER BY tstamp DESC`
	return fmt.Sprintf(q, tableName)
}

func (c *Clickhouse) AttachOptions(options map[string]string) error {
	if val, ok := options[paramOnCluster]; ok {
		if val == "true" {
			clusterMacro, ok := options[paramClusterMacro]
			if !ok {
				clusterMacro = "{cluster}"
			}
			c.Params.ClusterMacro = clusterMacro
			c.Params.OnCluster = true
		}
	}
	if val, ok := options[paramNoKeeperMap]; ok {
		if val == "true" {
			c.Params.NoKeeperMap = true
		}
	}

	return nil
}
