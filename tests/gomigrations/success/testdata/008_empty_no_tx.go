package gomigrations

import (
	"gitlab.com/gitlab-org/opstrace/goose/v3"
)

func init() {
	goose.AddMigrationNoTx(nil, nil)
}
