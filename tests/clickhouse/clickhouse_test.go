package clickhouse_test

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/opstrace/goose/v3"
	"gitlab.com/gitlab-org/opstrace/goose/v3/internal/check"
	"gitlab.com/gitlab-org/opstrace/goose/v3/internal/testdb"
)

func TestMain(m *testing.M) {
	if err := goose.SetDialect("clickhouse"); err != nil {
		log.Fatal(err)
	}
	os.Exit(m.Run())
}

func openClickHouse(t *testing.T) *sql.DB {
	t.Helper()

	workingDir, err := os.Getwd()
	require.NoError(t, err)
	ctx := context.Background()
	// Minimal additional configuration (config.d) to enable cluster mode
	replconf := path.Join(workingDir, "clickhouse-replicated.xml")
	chContainer, err := testdb.CreateClickHouseContainer(ctx, replconf)
	require.NoError(t, err)

	t.Cleanup(func() {
		if err := chContainer.Terminate(ctx); err != nil {
			t.Fatalf("failed to terminate container: %s", err)
		}
		// Reset the DB to default state - for some reason clearing up just the options doesn't work
		if err := goose.SetDialect("clickhouse"); err != nil {
			t.Fatalf("failed to set dialect: %s", err)
		}
	})

	port, err := chContainer.MappedPort(ctx, "9000/tcp")
	require.NoError(t, err)
	host, err := chContainer.Host(ctx)
	require.NoError(t, err)
	endpoint := fmt.Sprintf("clickhouse://%s:%s", host, port.Port())
	t.Log(endpoint)

	db, err := testdb.OpenClickhouse(endpoint, true)
	require.NoError(t, err)

	return db
}

func TestClickUpDownAll(t *testing.T) {
	t.Parallel()

	migrationDir := filepath.Join("testdata", "migrations")

	db := openClickHouse(t)
	/*
		This test applies all up migrations, asserts we have all the entries in
		the versions table, applies all down migration and asserts we have zero
		migrations applied.

		ClickHouse performs UPDATES and DELETES asynchronously,
		but we can best-effort check mutations and their progress.

		This is especially important for down migrations where rows are deleted
		from the versions table.

		For the sake of testing, there might be a way to modifying the server
		(or queries) to perform all operations synchronously?

		Ref: https://clickhouse.com/docs/en/operations/system-tables/mutations/
		Ref: https://clickhouse.com/docs/en/sql-reference/statements/alter/#mutations
		Ref: https://clickhouse.com/blog/how-to-update-data-in-click-house/
	*/
	provider, err := goose.NewProvider(
		goose.DialectClickHouse,
		db,
		os.DirFS(migrationDir),
	)
	require.NoError(t, err)

	// Collect migrations so we don't have to hard-code the currentVersion
	// in an assertion later in the test.
	migrations, err := goose.CollectMigrations(migrationDir, 0, goose.MaxVersion)
	check.NoError(t, err)

	currentVersion, err := goose.GetDBVersion(db)
	check.NoError(t, err)
	check.Number(t, currentVersion, 0)

	_, err = provider.Up(context.Background())
	check.NoError(t, err)
	currentVersion, err = goose.GetDBVersion(db)
	check.NoError(t, err)
	check.Number(t, currentVersion, len(migrations))

	_, err = provider.DownTo(context.Background(), 0)
	check.NoError(t, err)

	currentVersion, err = goose.GetDBVersion(db)
	check.NoError(t, err)
	check.Number(t, currentVersion, 0)
}

func TestClickHouseMissing(t *testing.T) {
	t.Parallel()

	migrationDir := filepath.Join("testdata", "migrations")
	db := openClickHouse(t)

	current, err := goose.EnsureDBVersion(db)
	require.NoError(t, err)
	require.Equal(t, current, int64(0))

	{
		migrations, err := goose.CollectMigrations(migrationDir, 0, 5)
		require.NoError(t, err)
		err = migrations[4].Up(db)
		require.NoError(t, err)
		currentVersion, err := goose.GetDBVersion(db)
		require.NoError(t, err)
		check.Number(t, currentVersion, 5)

	}

	{
		provider, err := goose.NewProvider(
			goose.DialectClickHouse,
			db,
			os.DirFS(migrationDir),
			goose.WithAllowOutofOrder(true),
		)
		require.NoError(t, err)
		_, err = provider.Up(context.Background())
		require.NoError(t, err)
		currentVersion, err := goose.GetDBVersion(db)
		require.NoError(t, err)
		check.Number(t, currentVersion, 4)
	}

	{
		anotherDir := filepath.Join("testdata", "missing-migrations")
		provider, err := goose.NewProvider(
			goose.DialectClickHouse,
			db,
			os.DirFS(anotherDir),
			goose.WithAllowOutofOrder(true),
		)
		require.NoError(t, err)

		_, err = provider.Up(context.Background())
		require.NoError(t, err)
		currentVersion, err := goose.GetDBVersion(db)
		require.NoError(t, err)
		check.Number(t, currentVersion, 6)
	}

}

func TestClickHouseFirstFive(t *testing.T) {
	t.Parallel()

	migrationDir := filepath.Join("testdata", "migrations")
	db := openClickHouse(t)

	provider, err := goose.NewProvider(
		goose.DialectClickHouse,
		db,
		os.DirFS(migrationDir),
	)
	require.NoError(t, err)
	_, err = provider.Up(context.Background())
	check.NoError(t, err)

	currentVersion, err := goose.GetDBVersion(db)
	check.NoError(t, err)
	check.Number(t, currentVersion, 5)

	type result struct {
		customerID     string    `db:"customer_id"`
		timestamp      time.Time `db:"time_stamp"`
		clickEventType string    `db:"click_event_type"`
		countryCode    string    `db:"country_code"`
		sourceID       int64     `db:"source_id"`
	}
	rows, err := db.Query(`SELECT * FROM clickstream ORDER BY customer_id`)
	check.NoError(t, err)
	var results []result
	for rows.Next() {
		var r result
		err = rows.Scan(&r.customerID, &r.timestamp, &r.clickEventType, &r.countryCode, &r.sourceID)
		check.NoError(t, err)
		results = append(results, r)
	}
	check.Number(t, len(results), 3)
	check.NoError(t, rows.Close())
	check.NoError(t, rows.Err())

	parseTime := func(t *testing.T, s string) time.Time {
		t.Helper()
		tm, err := time.Parse("2006-01-02", s)
		check.NoError(t, err)
		return tm
	}
	want := []result{
		{"customer1", parseTime(t, "2021-10-02"), "add_to_cart", "US", 568239},
		{"customer2", parseTime(t, "2021-10-30"), "remove_from_cart", "", 0},
		{"customer3", parseTime(t, "2021-11-07"), "checkout", "", 307493},
	}
	for i, result := range results {
		check.Equal(t, result.customerID, want[i].customerID)
		check.Equal(t, result.timestamp, want[i].timestamp)
		check.Equal(t, result.clickEventType, want[i].clickEventType)
		if result.countryCode != "" && want[i].countryCode != "" {
			check.Equal(t, result.countryCode, want[i].countryCode)
		}
		check.Number(t, result.sourceID, want[i].sourceID)
	}
}

func TestClickHouseOnCluster(t *testing.T) {
	err := goose.AttachOptions(map[string]string{
		"ON_CLUSTER": "true",
	})
	require.NoError(t, err)

	db := openClickHouse(t)

	_, err = goose.GetDBVersion(db)
	require.NoError(t, err)

	migrationDir := filepath.Join("testdata", "migrations")
	// Collect migrations so we don't have to hard-code the currentVersion
	// in an assertion later in the test.
	migrations, err := goose.CollectMigrations(migrationDir, 0, goose.MaxVersion)
	check.NoError(t, err)

	currentVersion, err := goose.GetDBVersion(db)
	check.NoError(t, err)
	check.Number(t, currentVersion, 0)

	provider, err := goose.NewProvider(
		goose.DialectClickHouse,
		db,
		os.DirFS(migrationDir),
	)
	require.NoError(t, err)
	_, err = provider.Up(context.Background())
	check.NoError(t, err)
	currentVersion, err = goose.GetDBVersion(db)
	check.NoError(t, err)
	check.Number(t, currentVersion, len(migrations))

	_, err = provider.DownTo(context.Background(), 0)
	check.NoError(t, err)

	currentVersion, err = goose.GetDBVersion(db)
	check.NoError(t, err)
	check.Number(t, currentVersion, 0)
}

func TestClickHouseNoKeeperMap(t *testing.T) {
	err := goose.AttachOptions(map[string]string{
		"NO_KEEPER_MAP": "true",
	})
	require.NoError(t, err)

	db := openClickHouse(t)

	_, err = goose.GetDBVersion(db)
	require.NoError(t, err)

	migrationDir := filepath.Join("testdata", "migrations")
	// Collect migrations so we don't have to hard-code the currentVersion
	// in an assertion later in the test.
	migrations, err := goose.CollectMigrations(migrationDir, 0, goose.MaxVersion)
	check.NoError(t, err)

	currentVersion, err := goose.GetDBVersion(db)
	check.NoError(t, err)
	check.Number(t, currentVersion, 0)

	provider, err := goose.NewProvider(
		goose.DialectClickHouse,
		db,
		os.DirFS(migrationDir),
	)
	require.NoError(t, err)
	_, err = provider.Up(context.Background())
	check.NoError(t, err)
	currentVersion, err = goose.GetDBVersion(db)
	check.NoError(t, err)
	check.Number(t, currentVersion, len(migrations))

	_, err = provider.DownTo(context.Background(), 0)
	check.NoError(t, err)

	currentVersion, err = goose.GetDBVersion(db)
	check.NoError(t, err)
	check.Number(t, currentVersion, 0)
}

func TestRemoteImportMigration(t *testing.T) {
	t.Parallel()
	// TODO(mf): use TestMain and create a proper "long" or "remote" flag.
	if !testing.Short() {
		t.Skip("skipping test")
	}
	// This test is using a remote dataset from an s3 bucket:
	// https://datasets-documentation.s3.eu-west-3.amazonaws.com/nyc-taxi/taxi_zone_lookup.csv
	// From this tutorial: https://clickhouse.com/docs/en/tutorial/
	// Note, these files are backed up in this repository in:
	// 		tests/clickhouse/testdata/backup-files/taxi_zone_lookup.csv
	// We may want to host this ourselves. Or just don't bother with SOURCE(HTTP(URL..
	// and craft a long INSERT statement.

	migrationDir := filepath.Join("testdata", "migrations-remote")
	db := openClickHouse(t)

	provider, err := goose.NewProvider(
		goose.DialectClickHouse,
		db,
		os.DirFS(migrationDir),
		goose.WithAllowOutofOrder(true),
	)
	require.NoError(t, err)
	_, err = provider.Up(context.Background())
	check.NoError(t, err)
	_, err = goose.GetDBVersion(db)
	check.NoError(t, err)

	var count int
	err = db.QueryRow(`SELECT count(*) FROM taxi_zone_dictionary`).Scan(&count)
	check.NoError(t, err)
	check.Number(t, count, 265)
}
